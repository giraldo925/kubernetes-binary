This pipeline builds kubernetes RPMs from official kubernetes binaries.  
You need write the version of kube in .gitlab-ci.yaml and after build, 
you can download RPMs from artifacts.