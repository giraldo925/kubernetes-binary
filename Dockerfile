FROM ruby:alpine
RUN apk --no-cache add ruby-dev build-base curl bash rpm
RUN gem install fpm
