#!/bin/bash
# https://github.com/apprenda-kismatic/kubernetes-distro-packages/blob/master/build_kubernetes.sh

K8S_VERSION=${K8S_VERSION:-1.8.6}
K8S_CLEAN_BUILD=${K8S_CLEAN_BUILD:-true}
PKG_ITERATION=${PKG_ITERATION:-1}
KUBERNETES_SERVER_ARCH=${KUBERNETES_SERVER_ARCH:-amd64}
KUBERNETES_SKIP_CONFIRM=true

if [[ "${K8S_CLEAN_BUILD}" != "false" ]]; then
  rm -rf kubernetes/source/kubernetes/v$K8S_VERSION
  rm -f kubernetes/builds/kubernetes-master-$K8S_VERSION-1.x86_64.rpm
  rm -f kubernetes/builds/kubernetes-node-$K8S_VERSION-1.x86_64.rpm
  rm -f kubernetes/builds/kubernetes-master_$K8S_VERSION_amd64.deb
  rm -f kubernetes/builds/kubernetes-node_$K8S_VERSION_amd64.deb
  rm -rf kubernetes/builds/systemd
fi

mkdir -p kubernetes/source/kubernetes/v$K8S_VERSION
cd kubernetes/source/kubernetes/v$K8S_VERSION
curl -L -O https://dl.k8s.io/v${K8S_VERSION}/kubernetes.tar.gz -z kubernetes.tar.gz
tar -xvzf kubernetes.tar.gz
KUBERNETES_SERVER_ARCH=${KUBERNETES_SERVER_ARCH} KUBERNETES_SKIP_CONFIRM=${KUBERNETES_SKIP_CONFIRM} kubernetes/cluster/get-kube-binaries.sh
tar xfvz kubernetes/server/kubernetes-server-linux-amd64.tar.gz
cd ../../../../

mkdir -p kubernetes/builds/systemd

function build_rpm_master {
# build_rpm_master

fpm -s dir -n "kubernetes-master" \
-p kubernetes/builds \
-v $K8S_VERSION \
-t rpm --rpm-os linux \
-a x86_64 \
--config-files etc \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "Kubernetes master binaries and services" \
--iteration ${PKG_ITERATION} \
--rpm-group "kube" \
--before-install scripts/before_install.sh \
--after-remove scripts/after_remove.sh \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kube-apiserver=/usr/bin/kube-apiserver \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kube-controller-manager=/usr/bin/kube-controller-manager \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kube-scheduler=/usr/bin/kube-scheduler \
services/kube-apiserver.service=/lib/systemd/system/kube-apiserver.service \
services/kube-controller-manager.service=/lib/systemd/system/kube-controller-manager.service \
services/kube-scheduler.service=/lib/systemd/system/kube-scheduler.service \
etc/apiserver=/etc/kubernetes/apiserver \
etc/config=/etc/kubernetes/config \
etc/controller-manager=/etc/kubernetes/controller-manager \
etc/scheduler=/etc/kubernetes/scheduler
}

function build_rpm_node {

fpm -s dir -n "kubernetes-node" \
-p kubernetes/builds \
-v $K8S_VERSION \
-a x86_64 \
-t rpm --rpm-os linux \
--config-files etc \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov <azalio@azalio.net>" \
--description "Kubernetes node binaries and services" \
--iteration ${PKG_ITERATION} \
--rpm-group "kube" \
--before-install scripts/before_install.sh \
--after-remove scripts/after_remove.sh \
etc/config=/etc/kubernetes/config \
etc/kubelet=/etc/kubernetes/kubelet \
etc/proxy=/etc/kubernetes/proxy \
services/kubelet.service=/lib/systemd/system/kubelet.service \
services/kube-proxy.service=/lib/systemd/system/kube-proxy.service \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kubelet=/usr/bin/kubelet \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kube-proxy=/usr/bin/kube-proxy
}

function build_rpm_client {

fpm -s dir -n "kubernetes-client" \
-p kubernetes/builds \
-a x86_64 \
--iteration ${PKG_ITERATION} \
-t rpm --rpm-os linux \
--config-files etc \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov <azalio@azalio.net>" \
--description "Kubernetes client" \
--version ${K8S_VERSION} \
--rpm-group "kube" \
kubernetes/source/kubernetes/v$K8S_VERSION/kubernetes/server/bin/kubectl=/usr/bin/kubectl

}

build_rpm_master
build_rpm_node
build_rpm_client
